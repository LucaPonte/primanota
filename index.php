<?php

include('login.php');

?>

<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link href="stile.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</head>
<body onload="load()">

<div class="bordo" id="main">
    <div>
        <label id="benvenuto" class="label">Benvenuto </label>
        <a id="logoutB" class="label" href="logout.php">Log Out</a>
    </div>
    <label id="saldo" class="label"></label>
    <div id="divTabella">

        <table id="table" class="tabella"></table>


        <div id="aggiungi">

            <label class="label">Importo:</label>
            <input class="campiAggiungi" type='number'  step="any" name='quantita' id="quantita">
            <label class="label">Tipo:</label>
            <input class="campiAggiungi" list='tipi' name='tipi' id="tipo" >
            <datalist id='tipi'>
            </datalist>
            <button id="buttonAggiungi" onclick="inserimento()">Aggiungi</button>
        </div>
    </div>
</div>


<script>
    function load(){

        var email="<?php echo $_SESSION['email']; ?>";

        $.ajax({ //aggiungi nome
            type: 'POST',
            url: 'funzioni.php',
            data: {funzione: "getNome", email:email},
            success: function (data) {
                data=JSON.parse(data);
                document.getElementById("benvenuto").innerHTML += data[0].nome +"!";

            }
        });


       aggiorna();

    }

    function aggiorna(){

        var email="<?php echo $_SESSION['email']; ?>";
        $.ajax({  // getInserimenti
            type: 'POST',
            url: 'funzioni.php',
            data: {funzione: "getInserimenti", email:email},
            success: function (data) {
                var saldo=0;
                var stringa="";
                data=JSON.parse(data);


                stringa+="<tr class='tabella'>"+
                "<td class='tabella'><label class='label'>IMPORTO</label></td>"+
                "<td class='tabella'><label class='label'>TIPO</label></td>"+
                "<td class='tabella'><label class='label'>DATA INSERIMENTO</label></td>"+
                "<td class='tabella'><label class='label'>MODIFICA</label></td>"+
                "<td class='tabella'><label class='label'>ELIMINA</label></td>"+
                "</tr>";


                for(var i in data){
                    var colore=(data[i].quantita>=0)?"class='verdeChiaro'":"class='rossoChiaro'";

                    stringa +="<tr id='"+data[i].ID+"tr' "+colore+" class='tabella'>"+
                    "<td id='"+data[i].ID+"q' class='tabella'><label class='label'>"+data[i].quantita+"</label></td>"+
                    "<td id='"+data[i].ID+"t' class='tabella'><label class='label'>"+data[i].tipo+"</label></td>"+
                    "<td id='"+data[i].ID+"d' class='tabella'><label class='label'>"+data[i].data+"</label></td>"+
                    "<td class='tabella'><label class='label'><img class='cursorClick' src='pencil-icon.png' width='25px' height='25px' onclick='modifica("+data[i].ID+")'></label></td>"+
                    "<td class='tabella'><label class='label'><img class='cursorClick' src='stop-icon.png' width='25px' height='25px' onclick='elimina("+data[i].ID+")'></label></td>"+
                    "</tr>";
                    saldo+= parseFloat(data[i].quantita);
                }
                document.getElementById("table").innerHTML=stringa;
                var colore=(saldo.toFixed()>0) ? ('id=\'saldoVerde\'') : ('id=\'saldoRosso\'')
                document.getElementById("saldo").innerHTML ="  Il tuo saldo attuale è:  "+
                "<label "+ colore +" class='label'>"+saldo.toFixed(1)+"</label>";

            }
        });

        $.ajax({  //aggiungi tipi
            type: 'POST',
            url: 'funzioni.php',
            data: {funzione: "getTipi", email:email},
            success: function (data) {
                data=JSON.parse(data);
                var stringa="";
                for(var i in data){
                    stringa+="<option value='"+ data[i].tipo+"'>";
                }
                document.getElementById("tipi").innerHTML= stringa
            }
        });
    }

    function inserimento(){
        var email="<?php echo $_SESSION['email']; ?>";
        var tipo=document.getElementById("tipo").value;
        var quantita= document.getElementById("quantita").value;
        $.ajax({
            type: 'POST',
            url: 'funzioni.php',
            data: {funzione: "addTipo", email:email, tipo:tipo},
            success: function (data) {

            }
        });

        $.ajax({
            type: 'POST',
            url: 'funzioni.php',
            data: {funzione: "nuovoInserimento", email:email, tipo:tipo , quantita:quantita},
            success: function (data) {
            }
        });

        aggiorna();
    }

    function modifica(id){
        var email="<?php echo $_SESSION['email']; ?>";
        $.ajax({  //aggiungi tipi
            type: 'POST',
            url: 'funzioni.php',
            data: {funzione: "getInserimento", id:id},
            success: function (data) {
                data=JSON.parse(data);
                document.getElementById(id+"q").innerHTML="<input class='campiAggiungi' type='number'  step='any' name='quantita' id='quantitaM' value='"+data[0].quantita+"'>";
                document.getElementById(id+"t").innerHTML="<input class='campiAggiungi' list='tipi' name='tipi' id='tipoM' value='"+data[0].tipo+"'>"+
                "<datalist id='tipiM'>"+
                "</datalist>";
            }
        });
        document.getElementById(id+"d").innerHTML="<button id='buttonM' onclick='inserimentoM("+id+")'>Modifica</button>";
        document.getElementById(id+"tr").style.backgroundColor= "transparent";

        $.ajax({  //aggiungi tipi
            type: 'POST',
            url: 'funzioni.php',
            data: {funzione: "getTipi", email:email},
            success: function (data) {
                data=JSON.parse(data);
                var stringa="";
                for(var i in data){
                    stringa+="<option value='"+ data[i].tipo+"'>";
                }
                document.getElementById("tipiM").innerHTML= stringa
            }
        });

    }

    function inserimentoM(id){

        var email="<?php echo $_SESSION['email']; ?>";
        var tipo=document.getElementById("tipoM").value;
        var quantita= document.getElementById("quantitaM").value;
        $.ajax({
            type: 'POST',
            url: 'funzioni.php',
            data: {funzione: "addTipo", email:email, tipo:tipo},
            success: function (data) {

            }
        });

        $.ajax({
            type: 'POST',
            url: 'funzioni.php',
            data: {funzione: "aggiorna", id:id, tipo:tipo , quantita:quantita},
            success: function (data) {

            }
        });

        aggiorna();
    }

    function elimina(id){
        $.ajax({
            type: 'POST',
            url: 'funzioni.php',
            data: {funzione: "deleteInserimento", id:id},
            success: function (data) {
            }
        });
        aggiorna();
    }

</script>

</body>
</html>